#include <iostream>

void PrintNumbers(int n, bool isEven)
{
    std::cout << "------\n";
    if (isEven) 
    {
        for (int i = 0; i <= n; i++)
        {
            if (i % 2)
            {
                continue;
            }
            std::cout << i << "\n";
        }
    }

    else 
    {
        for (int i = 0; i < n; ++i)
        {
            if (i % 2)
            {
                std::cout << i << "\n";
            }
        }
    }

    std::cout << "------\n";

    std::cout << " " << "\n";
}

int main()
{
    PrintNumbers(10, true); //�������� ������ �� 10
    PrintNumbers(10, false); //�� �� �������, �� � ������� ���������� - ������� ��� �������� �� 10
}